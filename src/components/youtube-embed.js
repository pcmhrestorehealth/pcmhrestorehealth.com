import React from "react"
import "./youtube-embed.css"
const Youtube = ({ video }) => (
  <div className='embed-container'>
      <iframe src={video} title="youtube video" frameBorder='0' allowFullScreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
  </div>
)
export default Youtube