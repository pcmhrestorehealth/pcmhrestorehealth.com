---
title: Team
---

import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import Members from "../data/members.yml"

export const pageQuery = graphql`
  query teamMembers {
  allMembersYaml {
    edges {
      node {
        name
        image {
          childImageSharp {
            fluid (maxWidth: 150, maxHeight: 150) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`

<section id="team" className="align-center">
<h1> Our team</h1>

<div className="members-container">
{props.data.allMembersYaml.edges.map(({node}) =>
    <div className='member-container'>
      <div className="member-image">
        <Img fluid={node.image.childImageSharp.fluid} />
      </div>
      <div className="member-name">
        { node.name }
      </div>
    </div>
)}
</div>

</section>

