---
title: COVID-19 Response
---

import Styles from "./covid-19.module.css"

## COVID-19 response kit

### Guidelines for elderly

<div className={Styles.guidelines}>
    <a className={Styles.guideline} href="https://docs.google.com/document/d/1pEgraE4c0bNnTYsYhilzdascqIRkoOAYXCRPXGky0dc/export?format=pdf">
      Experiencing Symptoms
    </a>
    <a className={Styles.guideline} href="https://docs.google.com/document/d/1jntBsyu3TQY57pA4voDS9ITaQIZ_pub-1ouSor8Hpwc/export?format=pdf">
      Precautionary Measures
    </a>
    <a className={Styles.guideline} href="https://drive.google.com/file/d/1LwejwtsoeyD802rWJWBOBNNk69VDdPnv/view?usp=sharing">
      Psychology Support
    </a>
    <a className={Styles.guideline} href="https://drive.google.com/file/d/1FId_6zXxVig5tmA6k8dQgqGP_XwMslj7/view?usp=sharing">
      Post COVID Management
    </a>
</div>

### Hand-washing

![Poster on Hand Washing, courtesy Google](../../images/google-hand-wash-poster.png)

### Cough etiquette

![Cover your cough, courtesy CDC](../../images/cdc-cover-your-cough.png)
