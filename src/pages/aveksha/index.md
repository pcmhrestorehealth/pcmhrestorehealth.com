---
title: "AVEKSHA - Reviving Home Based Care"
redirect_from:
  - /elderly-care
  - /home-health-care
---

import { graphql, Link } from 'gatsby'

import AvekshaPostLink from "../../components/aveksha-post-link"
import Layout from "../../components/layout-aveksha"
import Styles from "../../components/aveksha.module.css"

export const pageQuery = graphql`
  query AvekshaBlogPosts {
    allSitePage (
      filter: { context: { frontmatter: { collection: { eq: "blog" }, category: { eq: "aveksha" } } } }
      sort: { order: DESC, fields: context___frontmatter___date }
    ) {
      edges {
        node {
          id
          context {
            frontmatter {
              date
              path
              title
            }
          }
        }
      }
    }
  }
`

export default Layout

<div className={Styles.avekshaLogo}>

<Link to="/aveksha">

![](../../images/Aveksha_Logo.001.jpeg)

</Link>

</div>

<div className={Styles.avekshaHeading}>

## Aveksha ##

</div>

<div className={Styles.pcmhLogo}>

<Link to="/">

![](../../images/biglogo.png)

</Link>

</div>

<div className={Styles.subTitle}>

Home Based Primary Care

</div>
<div className={Styles.tagLine}>

"<span>Cure Sometimes</span>, <span>Treat Often</span>, <span>Comfort Always</span>" --Hippocrates

</div>


<nav className={Styles.navBar}>

* <Link to="/">PCMH Home</Link>
* [About](#about)
* [Services](#services)
* [Stories](#stories)

</nav>

<div className={Styles.alertBox}>
<Link to="/aveksha/covid-19"><span className={Styles.blink}>COVID-19 resources</span></Link>
</div>

<div className={Styles.book}>
<a href="tel:+918971544066">📱8971544066</a>
</div>

<div id="vision" className={[Styles.visionArea, Styles.writeUp].join(' ')}>
<h2>Vision</h2>
<p>We envision healthy communities through teams providing person centred primary care with passion at the comfort of people's home..</p>
<h2>Mission</h2>
<p>To deliver world class, compassionate and comprehensive health care services to people and improve their quality of life by adding life to years and not merely years to life. Inspire primary care practitioners to take up this form of holistic Home Based Primary Care as their own mission and grow through them.</p>
</div>

<div id="services" className={Styles.servicesWrapper}>
<h2>Services</h2>
<div className={Styles.services}>

<div className={Styles.service}>

![](../../images/palliative-care.svg)

<h3>Palliative Care</h3>
<p>Symptom management, end of life care, cancer, bereavement</p>
</div>
<div className={Styles.service}>

![](../../images/medication-optimization.svg)

<h3>Medication Management</h3>
<p>Poly-pharmacy, drug optimization</p>
</div>
<div className={Styles.service}>

![](../../images/life-style-management.svg)

<h3>Lifestyle Management</h3>
<p>Chronic disease management, nutrition and exercise, elderly care</p>
</div>
<div className={Styles.service}>

![](../../images/post-discharge.svg)

<h3>Post-discharge Follow-up</h3>
<p>Recovery assessment, regular medical checkup</p>
</div>
<div className={Styles.service}>

![](../../images/mental-well-being.svg)

<h3>Mental Well-being</h3>
<p>Psychometric assessment, counseling, psychotherapy (anxiety, depression, phobias, etc.)</p>
</div>
<div className={Styles.service}>

![](../../images/minor-medical-procedure.svg)

<h3>Minor Medical Procedures</h3>
<p>Debridement, bed sores, wound care, vaccination, catheterization, injections</p>
</div>

</div>
</div>

<div id="about" className={[Styles.writeUp, Styles.writeUpArea].join(' ')}>
<h2>Our Story</h2>
<p>Aveksha the Home Based Primary Care Program initiated in April 2019 was conceived to revive and redefine approach to home based care with the principles of family medicine and palliative care; attending to the entire spectrum of cradle to grave.</p>
<p>Initiated as a physician-pharmacist team, we are dedicated to creating a home health care tribe through training medical and allied healthcare professionals.</p>
<p>Our inspiration is driven from the Patient Centred Uber model where the canvas is focused mainly on the patient/s with their family in the centre and a team of medical professionals to provide quality health at the comforts of their homes.</p>
<p>Aveksha was conceived to bring home based health care to the entire spectrum of living: from the newborn to the elderly.</p>
<p>We initiated services in April 2019 led by a physician-clinical pharmacist team and have seen great response and need for such a service in the community.</p>
<p>Our inspiration is drawn from patients and their families and we strongly practise shared decision making in health care plans.</p>
<p>We are invested in grooming the upcoming health professionals in home health care through fellowship and internship opportunities.</p>


![](../../images/aveksha-model.png)

</div>

<div id="stories" className={Styles.blog}>
<h2>Stories</h2>
<p>{props.data.allSitePage.edges.length > 0 && props.data.allSitePage.edges.map(edge => <AvekshaPostLink post={edge.node.context} />)}</p>
</div>

<div className={Styles.book2}>
<a href="tel:+918971544066">📱8971544066</a>
</div>

<footer className={Styles.footer}>
<hr></hr>
<div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>, <a href="https://www.flaticon.com/authors/geotatah" title="geotatah">geotatah</a>, <a href="https://www.flaticon.com/authors/wanicon" title="wanicon">wanicon</a>, and <a href="https://www.flaticon.com/authors/flat-icons" title="Flat Icons">Flat Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></div>
</footer>
