---
title: EMPOWER YOUR LIFE
---

# EMPOWER YOUR LIFE #
## COMPREHENSIVE MANAGEMENT OF LONG STANDING MEDICAL CONDITIONS THROUGH PERSON CENTERED APPROACH ##

Self-management of long standing medical condition through person focused approach is a modality of care that PCMH restore health clinic facilitates people through a multi-interventional and integrated approach of health care.


<div style="margin: 1rem; background-color: cyan; border-radius:10px; padding: 3rem ">

### Patient Story

Mr K, 45 year old visited doctor with complaints of tiredness, weakness and frequent urination, the doctor advised him to take a few tests and he was diagnosed with diabetes and thyroid issues. Doctor advised him to take 5 medications every day along with few diet modifications. K went walking back home and it kept ringing in his ear, "you will have to take these medications for a lifetime!” He felt sick thinking about it. How he had been well until this morning but this consultation and diagnosis made him feel like a patient. 

"Will I be able to eat the laddoos I like? Will I be able to travel without worrying anymore? What about my daughter, would she get it as well because of me? Is my health under my control anymore? ”and several other questions ran through his mind. It was difficult to explain how he felt to his wife/family but he followed the doctor's advice very well. He took the 5 tablets every day and made a few changes in his diet, removed sugar and sweets from his life. In spite of these changes, he wasn't sure he felt better or felt under control. He often lost in thoughts like "why did I get this? What will happen to me?”  This eventually led him to have depression, not able to fully enjoy social gatherings and engage with family like before. Wife sensed this discomfort in the husband and they felt they needed more guidance on how to deal with his condition and also answer their apprehensions. 

</div>

### What is a long standing medical condition?

A long standing medical condition, called as chronic diseases are those that are likely to be present for a longer time. A long standing medical condition could be communicable diseases like HIV, Hepatitis B and C or non-communicable health conditions like diabetes, hypertension psychological concerns such as depression etc. They result from a combination of physiological, psychological, environmental and genetic factors and hence the management of chronic illness involves taking into account all these factors and caring for the individual.

Often chronic condition can be controlled and may or may not have complete cure and thus has a huge impact on you and your family’s life in many ways.     

### Examples of chronic illness:

Non communicable diseases (NCD) are diseases that are not transmitted directly from one person to another. Some examples of non-communicable diseases are:

* Diabetes
* Hypertension
* Mood disorders
* Personality disorders
* Cardiovascular diseases
* Asthma
* Cancer, etc

Communicable disease is a disease that is transmitted directly from one person to another. Few examples of communicable diseases are:

* HIV and AIDS
* Hepatitis B and C

### Why  chronic disease condition is a threat?

NCDs dominate the list of the top causes of death in India. In 2016 a staggering 28.1% of all deaths in India were caused by heart conditions. Overall, 61.8% of all deaths in the country were caused by NCDs in 2016. **One in four Indians** are at risk of dying before the age of seventy due to an NCD (Health issues India, 2019).

In the above given example Mr. K  had to go through several challenges and the tension which was created due to the chronic condition lead him to self-isolation and depression having a huge impact on him and  his family’s  life.

![](../images/chronic-illness-cycle.jpg)

**Fig 1: Health deterioration cycle with long standing Medical condition**

![](../images/chronic-illness-needs.png)

**Fig 2: Explains the physical and psychological needs of people with chronic conditions**

## Our Model

In line with the chronic care model, our care approach  comprises of a mental health provider, psychologist for counseling and therapy along with our team of clinical health provider, a family physician and clinical pharmacologist where we diagnose your condition (Assess) work with you to identify your needs and concerns (Assist), work together to create a personalized care plan (arrange and action).

![](../images/chronic-illness-model.png)

## Take home messages:

* Long standing medical conditions require guided care for effective management of health.
* Pills alone sometimes do not cure a condition as they require a customized and consistent multidisciplinary care.
* Peaceful lifestyle by enhancing emotional and mental wellbeing through holistic care is a requisite for quality living with the long standing medical condition.
* Pharmacological assessment of pill burden and personalized care plan is an added advantage to the advancement of better health.

*“When a long standing medical condition is a way of illness, managing it is a way of life”*

### Bibliography

<div style="margin: 2rem; text-indent: -2rem">

Health issues India. (2019). Health issues india. Retrieved november 16, 2019, from Health issues india: https://www.healthissuesindia.com/noncommunicable-diseases/

</div>
