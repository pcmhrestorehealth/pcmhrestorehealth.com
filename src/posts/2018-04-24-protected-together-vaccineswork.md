---
title: "Protected Together #VaccinesWork"
---
protected-together-vaccineswork

April 24th - 30th is recognized by WHO as World Immunization Week 2018. Immunization saves millions of lives and is world’s most successful and cost-effective health interventions. However, more than 19 million unvaccinated children or under-vaccinated around the world,  putting them at serious risk of these potentially fatal diseases. World Immunization Week aims to ensure every person in the world is protected from vaccine preventable diseases. 'Protected together, #VaccinesWork' is this year's theme, encouraging all sections of the society to support and understand the importance of vaccination so that we can raise the standards of health globally.

![](../images/protected-together-vaccineswork-1.gif)

As part of the 2018 campaign, WHO and partners aim to:
* Highlight the importance of immunization, and the remaining gaps in global coverage
* Underscore the value of vaccines to target donor countries and the importance of investing in immunization efforts
* Highlight the ways in which everyone – from donors to individuals – can and must drive vaccine progress.

![](../images/protected-together-vaccineswork-2.webp)
![](../images/protected-together-vaccineswork-3.webp)

PCMH Restore Health has since its conception, promoted vaccinations. In keeping with WHO's goals, our team will continue to strive to ensure that the benefits of vaccination are received by as many people as possible. We encourage you to join this global movement by getting yourselves, your family and friends vaccinated and educated so that we can move towards a healthier society by disease elimination and eradication.
![](../images/protected-together-vaccineswork-4.gif)

Share this on social media platforms to spread awareness and to highlight the importance of protection against vaccine preventable diseases.
