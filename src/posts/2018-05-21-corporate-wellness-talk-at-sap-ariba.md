---
title: "Corporate Wellness talk at SAP Ariba"
---

 To further our corporate wellness campaign, our next stop was at SAP Ariba located in Bellandur. We focused on two important concepts for a healthy lifestyle – Adult vaccination and preventing 'Burn Out'.

Dr. Shalini who spearheads our family medicine department spoke about adult vaccination, it’s importance and needs. The importance that we give to adult vaccines pales in comparison to the effort we take to ensure our children are vaccinated. Dr. Shalini spoke about how vaccines are not to be limited to children but are essential to adults especially the elderly, to ensure a healthy life. In fact, the Center for Disease Control (CDC) recommends vaccines for adults and the elderly. For more on vaccines visit: https://www.pcmhrestorehealth.com/adult-vaccination

![](../images/corporate-wellness-talk-at-sap-ariba-1.webp)
![](../images/corporate-wellness-talk-at-sap-ariba-2.webp)

This was followed by a session on ‘burn out’ by Mr. Aji Joseph, psychologist.

Burnout is exhaustion a person experiences in response to chronic emotional stressors arising from life management difficulties. In our busy schedules it is essential to be able to identify events (often overlapping) that lead to burn out. Mr. Aji pointed out signs to look out for to identify and later went on to discuss how to prevent ( psychological wellness ) and how to deal with burnout. To know more about  psyche-excellence services visit: https://www.pcmhrestorehealth.com/psychological-services


![](../images/corporate-wellness-talk-at-sap-ariba-3.webp)
![](../images/corporate-wellness-talk-at-sap-ariba-4.webp)
