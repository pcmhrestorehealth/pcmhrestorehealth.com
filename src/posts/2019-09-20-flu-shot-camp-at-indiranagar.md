---
title: Flu Shot Camp at Indiranagar
---

PCMH Restore Health successfully conducted an influenza immunization camp at Indiranagar today. 20 people were vaccinated in this camp. Thanks to Dr BC Rao for providing the venue and spreading the word.